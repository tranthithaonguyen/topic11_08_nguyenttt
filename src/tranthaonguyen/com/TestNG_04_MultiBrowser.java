package tranthaonguyen.com;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.AssertJUnit;
import org.testng.annotations.Test;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;

import static org.testng.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class TestNG_04_MultiBrowser {
	private WebDriver driver;
	
	@org.testng.annotations.Parameters("browser")
	 @BeforeClass
	  public void beforeClass(String browsername) {
		System.out.println("browsername"+browsername);
		if(browsername.equals("chrome"))
		{
			
			 System.setProperty("webdriver.chrome.driver",".\\lib\\chromedriver.exe");
			  driver=new ChromeDriver();
		}
		else if(browsername.equals("firefox"))
		{
			driver=new FirefoxDriver();
		}
		 
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	  }
	@org.testng.annotations.Parameters({"user", "pass"})
	
	  @Test()
	  public void TC_01_Login(String user,String  pass ) {
		  
		  driver.get("http://live.guru99.com/index.php/customer/account/login/");
		  driver.findElement(By.xpath("//input[@id='email']")).sendKeys(user);
		  driver.findElement(By.xpath("//input[@id='pass']")).sendKeys(pass);
		  driver.findElement(By.xpath("//button[@id='send2']")).click();
		  
		  AssertJUnit.assertTrue(driver.findElement(By.xpath("//h1[text()='My Dashboard']")).isDisplayed());

		  AssertJUnit.assertTrue(driver.findElement(By.xpath("//div[@class='box-content']/p[contains(.,'" +user + "')]")).isDisplayed());
		  
		  driver.findElement(By.xpath("//div[@class='account-cart-wrapper']//span[text()='Account']")).click();
		  driver.findElement(By.xpath("//a[text()='Log Out']")).click();
		  AssertJUnit.assertTrue(driver.findElement(By.xpath("//h2[contains(text(),'This is demo site for')]")).isDisplayed());
		  
	  }
	 
	 
	
	  @AfterClass
	  public void afterClass() {
	  }

 

}
