package tranthaonguyen.com;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.sql.Time;
import java.util.concurrent.TimeUnit;

import org.jboss.netty.util.Timeout;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;

public class TestNG_06_Dependencies {
	private WebDriver driver;
	
	  @BeforeClass
	  public void beforeClass() {
		  System.setProperty("webdriver.chrome.driver",".\\lib\\chromedriver.exe");
		  driver=new ChromeDriver();
		  
		  driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
		  driver.manage().window().maximize();
	  }

	  
  @Test(dataProvider = "UserAndPass")
  public void Login(String user, String pass) {
	  driver.get("http://live.guru99.com/index.php/customer/account/login/");
	  driver.findElement(By.xpath("//input[@id='email']")).sendKeys(user);
	  driver.findElement(By.xpath("//input[@id='pass']")).sendKeys(pass);
	  driver.findElement(By.xpath("//button[@id='send2']")).click();
	  
	  assertFalse(driver.findElement(By.xpath("//h1[text()='My Dashboard']")).isDisplayed());
	  System.out.println(user);
	  assertTrue(driver.findElement(By.xpath("//div[@class='box-content']/p[contains(.,'" + user + "')]")).isDisplayed());
	  
//	  driver.findElement(By.xpath("//div[@class='account-cart-wrapper']//span[text()='Account']")).click();
//	  driver.findElement(By.xpath("//a[text()='Log Out']")).click();
//	  assertTrue(driver.findElement(By.xpath("//h2[contains(text(),'This is demo site for')]")).isDisplayed());
  }
  @Test(dependsOnMethods="Login")
  public void TC_02(String user, String pass) {
	  
	  driver.findElement(By.xpath("//div[@class='account-cart-wrapper']//span[text()='Account']")).click();
	  driver.findElement(By.xpath("//a[text()='Log Out']")).click();
	  assertTrue(driver.findElement(By.xpath("//h2[contains(text(),'This is demo site for')]")).isDisplayed());
  }
  
  @DataProvider
  public Object[][] UserAndPass() {
    return new Object[][] {
      { "nguyen04@gmail.com", "1234567890" },
      { "nguyen05@gmail.com", "1234567890" },
    };
  }

  @AfterClass
  public void afterClass() {
  }

}
