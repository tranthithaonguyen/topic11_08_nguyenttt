package tranthaonguyen.com;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class TestNG_01_Annotations {
	
	
	
	 @DataProvider
	  public Object[][] dp() {
	    return new Object[][] {
	      new Object[] { 1, "a" },
	      new Object[] { 2, "b" },
	    };
	  }
	
	  @BeforeSuite
	  public void beforeSuite() {
		  System.out.println("beforeSuite");
	  }
	  
		  @BeforeTest
		  public void beforeTest() {
			  System.out.println("beforeTest");
		  }
		  @BeforeClass
		  public void beforeClass() {
			  System.out.println("@BeforeClass");
		  }
	  
  
			  @BeforeMethod
			  public void beforeMethod() {
				  System.out.println("@BeforeMethod");
			  }
					  @Test(dataProvider = "dp")
					  public void f(Integer n, String s) {
						  System.out.println("void f");
					  }
			  @AfterMethod
			  public void afterMethod() {
				  System.out.println("@AfterMethod");
			  }

  @AfterClass
  public void afterClass() {
	  System.out.println("afterClass");
  }

  @AfterTest
  public void afterTest() {
	  System.out.println("afterTest");
  }

  @AfterSuite
  public void afterSuite() {
	  System.out.println("afterSuite");
  }

}
